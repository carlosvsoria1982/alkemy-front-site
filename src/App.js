import './App.css';
// import { Container, Navbar, Nav } from 'react-bootstrap'
// import Header1 from '../src/Header/Header'
 import Balance from '../src/Components/home/Balance'
 import Header1 from './Components/Header/Header';
 import NewRegister from './Components/NewRegister/NewRegister';
 import Details from './Components/Details/Details';
 import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

 function App() {
  return (
    <div className="App">

      <header className="App-header">
      <Header1/>
      <Router>
          
        <Switch>
              <Route path="/" exact component={Balance} />
              <Route path="/newregister" exact component={NewRegister} />
              <Route path="/details" exact component={Details} />
        
        </Switch>
        </Router>
      </header>
    </div>
  );
}

export default App;
