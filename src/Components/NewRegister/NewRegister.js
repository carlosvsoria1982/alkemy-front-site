import React, { useState, useEffect, Fragment } from 'react'
import { Form, Button, Toast, Row, Col, Modal, FloatingLabel } from 'react-bootstrap'


function NewRegister() {


  const [datos, setDatos] = useState(
    {
      type: "in",
      qty: "12",
      concept: "33",
      date: "24-03-21"
    }
  )
  const requestOptions = {
    method: 'POST',
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify(datos)
  };
  console.log(requestOptions.body)

  const handleInputChange = (event) => {
    // console.log(event.target.value)
    setDatos({
      ...datos,
      [event.target.name]: event.target.value
    })
    console.log(datos)
  }

  const enviarDatos = (event) => {
    event.preventDefault()
    console.log(datos)
    console.log("haciendo post")
    // requestOptions[body]=JSON.stringify(datos)
    fetch("http://localhost:3001/v1/movements/add",requestOptions)
    .then(response => response.json())
    .then(responseData =>{
        console.log(responseData)
    })
  }
  // const [value, onChange] = useState(new Date());

  useEffect(function () {
    console.log(datos.date)

  })
  return (
    <div>
      <h1>NewRegister</h1>
      <br />
      <Fragment>
        <form classname="row" onSubmit={enviarDatos}>
          <div classname="col-md-6">
            <FloatingLabel controlId="floatingSelect" label="Tipo">
              <Form.Select
                aria-label="Floating label select example"
                classname="form-control"
                type="text"
                name="type"
                onChange={handleInputChange}
              >
                <option value="in">Ingreso</option>
                <option value="out" select>Egreso</option>
              </Form.Select>
            </FloatingLabel>
          </div>
          <div classname="col-md-6">
            <Form.Control
              placeholder="ingresar el monto"
              classname="form-control"
              type="text"
              name="qty"
              onChange={handleInputChange}
              showTimeSelect
              dateFormat="Pp"
            />

          </div>
          <div classname="col-md-6">
            <Form.Control
              placeholder="ingresar el concepto"
              classname="form-control"
              type="text"
              name="concept"
              onChange={handleInputChange}
            />

          </div>
          <div classname="col-md-6">
            <Form.Control
              placeholder="ingresar la fecha"
              classname="form-control"
              type="text"
              name="date"
              onChange={handleInputChange}
            />

          </div>
          
 
          <div classname="col-md-6">
            <Button variant="secondary" type="submit">
              enviar
            </Button>
          </div>

        </form>

      </Fragment>

    </div>
  )
}

export default NewRegister