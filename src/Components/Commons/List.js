import React from 'react'
import { Table, InputGroup } from 'react-bootstrap'
import { Delete, Edit } from '@material-ui/icons'
function List({ data, selection }) {
    console.log(data)
    return (
        <div>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        {
                            selection &&
                            <th>Selección</th>
                        }

                        <th>Monto</th>
                        <th>Concepto</th>
                        <th>Operacion</th>
                        <th>Fecha</th>
                        {
                            selection &&
                            <th>Edición</th>
                        }
                    </tr>
                </thead>


                {
                    data.map((data, key) => {
                        var temp = data.date.split("T")
                        data.date = temp[0]
                        return (
                            <tbody key={key}>
                                <tr>
                                    {
                                        selection &&
                                        <td>
                                            <InputGroup >
                                                <InputGroup.Checkbox />


                                            </InputGroup>
                                        </td>
                                    }


                                    <td>
                                        ${data.qty}
                                    </td>

                                    <td>
                                        {data.concept}
                                    </td>

                                    <td>
                                        {data.type}
                                    </td>

                                    <td>
                                        {data.date}
                                    </td>
                                    {
                                        selection &&
                                        <td> <Delete/> <Edit/>
                                        </td>
                                    }
                                </tr>


                            </tbody>
                        )
                    })
                }

            </Table>
        </div>
    )
}

export default List