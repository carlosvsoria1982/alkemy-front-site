import React, {useState, useEffect} from 'react'
import { data, lastTen } from '../../Data/exampleData'
import List from '../Commons/List'
import { Table, InputGroup, Container, Col, Row } from 'react-bootstrap'
function Balance() {
var stateBalance = 0
const [showBalance, setShowBalance] = useState("")
const [listData, setListData] = useState("")
const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ limit: 10 })
};
useEffect (function(){
    console.log("useEffect entrando...")
    fetch("http://localhost:3001/v1/balance")
    .then(response => response.json())
    .then(responseData =>{
        console.log(responseData)
        // console.log(responseData[0].balance)
        setShowBalance(responseData)
    })
    fetch("http://localhost:3001/v1/movements", requestOptions)
    .then(response => response.json())
    .then(responseData =>{
        console.log(responseData)
        setListData(responseData)
       
    })
    
    
    
},[])


return (
        <div>
            <Container>
                <Row>
                    <>
                    {
                        showBalance&&
                        <>
                        <br/>
                        <br/>
                        <br/>
                        <div/>
                        <div/>
                        
                            <h1>
                                Balance actual: ${showBalance[0].balance}
                            </h1>

                        </>
                    }
                    </>
                    
                </Row>
                <Row>
                    <h2>
                        Últimos movimientos
                    </h2>
                    {
                        listData &&
                        <List 
                        data = {listData}
                        selection = {false}
                />
                    }

                </Row>

            </Container>
 
        </div>
    )
}

export default Balance