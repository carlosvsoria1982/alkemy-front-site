import React from 'react';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap'
function Header1() {
  return (
    <div>

      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/">Presupuesto Personal</Navbar.Brand>
          <Nav.Link href="/newregister">Nuevo Registro</Nav.Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">

              <NavDropdown title="Detalles" id="collasible-nav-dropdown">
                <NavDropdown.Item href="/details">Ingresos</NavDropdown.Item>
                <NavDropdown.Item href="/details">Egresos</NavDropdown.Item>

              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  )
}

export default Header1;